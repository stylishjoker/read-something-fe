import HomePage from "../Pages/home";
import Login from "../Pages/login";
import Me from "../Pages/me";
import ChangePassword from "../Pages/me/change-password";
import ComicFollowed from "../Pages/me/comic-followed";
import StoryPage from "../Pages/story";
import Detail from "../Pages/story/detail";
import DetailChap from "../Pages/story/detail-chap";
import { MeLayout } from "../components/layout/me-layout";

export const routers = [
  {
    name: "home",
    component: HomePage,
    private: true,
    path: "home",
  },
  {
    name: "story",
    component: StoryPage,
    private: true,
    path: "story",
  },
  {
    name: "follow",
    component: StoryPage,
    private: true,
    path: "follow",
  },
  {
    name: "history",
    component: StoryPage,
    private: true,
    path: "history",
  },
  {
    name: "login",
    component: Login,
    private: false,
    path: "login",
  },
  {
    name: "me",
    component: Me,
    private: false,
    path: "me",
    layout: MeLayout,
  },
  {
    name: "detail",
    component: Detail,
    private: false,
    path: "detail/:id",
  },
  {
    name: "detail-chap",
    component: DetailChap,
    private: false,
    path: "detail-chap/:id",
  },
  {
    name: "comic-followed",
    component: ComicFollowed,
    private: false,
    path: "/me/comic-followed",
    layout: MeLayout,
  },
  {
    name: "detail",
    component: ChangePassword,
    private: false,
    path: "/me/change-password",
    layout: MeLayout,
  },
];
