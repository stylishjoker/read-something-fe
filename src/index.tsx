import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { AuthProvider } from "./hook/useAuth";
import { CookiesProvider } from "react-cookie";
import { StoryProvider } from "./hook/useStory";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <BrowserRouter>
    <CookiesProvider defaultSetOptions={{ path: "/" }}>
      <AuthProvider>
        <StoryProvider>
          <App />
        </StoryProvider>
      </AuthProvider>
    </CookiesProvider>
  </BrowserRouter>
);
