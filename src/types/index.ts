export interface IChap {
    id: string;
    name: string;
    createdAt: string;
    description: string;
    images: string[];
}
export interface IDetailStory {
    id: string;
    title: string;
    author: string;
    category: string;
    follow: string[];
    status: string;
    totalView: number;
    createdAt: number;
    chap: IChap[];
    image: string;
}

