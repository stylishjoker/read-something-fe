import { Route, Routes } from "react-router-dom";
import { routers } from "./routers";
import { Header } from "./components/header";

function App() {
  return (
    <>
      <Header />
      <main className="mx-auto pt-[80px] md:w-[80%]">
        <Routes>
          {routers.map((item) => {
            const Layout = item.layout;
            return (
              <Route
                key={item.name}
                path={`/${item.path}`}
                element={
                  Layout ? (
                    <Layout>
                      <item.component />
                    </Layout>
                  ) : (
                    <item.component />
                  )
                }
              />
            );
          })}
        </Routes>
      </main>
    </>
  );
}

export default App;
