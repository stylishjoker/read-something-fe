import instance from "../lib/http";

export class ChatRequest {
    async getDetailChap(id: string) {
        try {
            const res = (await instance.get(`/chap/${id.trim()}`)).data;
            return res;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }
}

export const chapRequest = new ChatRequest();