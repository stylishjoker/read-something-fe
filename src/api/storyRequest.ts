import instance from "../lib/http";

export class StoryRequest {
  async getDetailStory(id: string) {
    try {
      const res = (await instance.get(`/story/${id.trim()}`)).data;
      return res;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}
export const storyRequest = new StoryRequest();
