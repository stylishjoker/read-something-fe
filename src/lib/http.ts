import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Cookies } from "react-cookie";

const cookie = new Cookies();

export const accessToken = cookie.get("accessToken");

const instance = axios.create({
  baseURL: "https://read-something-be.vercel.app/api/",
  // baseURL: "http://localhost:3000/api/",
  headers: {
    Authorization: accessToken ? "Bearer " + accessToken : "",
  },
});

axios.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error: AxiosError) {
    if (axios.isAxiosError(error)) {
      // This is an Axios error
      console.error("Axios error:", error.message);
      console.error("Response data:", error.response?.data);
      // Handle Axios error here
    } else {
      // This is a regular JavaScript error
      console.error("Regular error:", error);
      // Handle other errors here
    }

    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    console.log(error);

    return Promise.reject(error);
  }
);

export interface AxiosResponse<T = never> {
  code: any;
  data: T;
  status: number;
  statusText: string;
  headers: Record<string, string>;
  config: AxiosRequestConfig<T>;
  request?: any;
}

export default instance;
