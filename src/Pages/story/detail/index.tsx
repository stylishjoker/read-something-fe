import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { storyRequest } from "../../../api/storyRequest";
import { Fragment } from 'react'
import { StarIcon } from '@heroicons/react/20/solid'
import { Tab } from '@headlessui/react'
import { IDetailStory } from "../../../types";
import moment from "moment";

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ')
}

export default function Detail() {
  const { id } = useParams();
  const [data, setData] = useState<IDetailStory>();
  console.log(id);
  console.log(data);

  useEffect(() => {
    const getData = async () => {
      const result = await storyRequest.getDetailStory(id?.toString() || "");
      console.log("story", result);
      setData(result?.data);
    };
    getData();
  }, []);

  return (
    <div className="bg-white">
      {data && (
        <div className="mx-auto px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
          <div className="lg:grid lg:grid-cols-7 lg:grid-rows-1 lg:gap-x-8 lg:gap-y-10 xl:gap-x-16">
            <div className="lg:col-span-4 lg:row-end-1 flex items-center justify-center">
              <div className="w-[400px] overflow-hidden rounded-lg bg-gray-100">
                <img src={data.image} alt='img' className="w-full h-full" />
              </div>
            </div>

            <div className="mt-14 max-w-2xl sm:mt-16 lg:col-span-3 lg:row-span-2 lg:row-end-2 lg:mt-0 lg:max-w-none">
              <div className="flex flex-col-reverse">
                <div className="">
                  <h1 className="text-2xl font-bold tracking-tight text-gray-900 sm:text-3xl">{data.title}</h1>
                  <p className="mt-2 text-sm text-gray-500">
                    <time dateTime="">{moment(data.createdAt).format('DD-MM-YYYY HH:mm:ss')}</time>
                  </p>
                </div>
              </div>
              <div className="mt-5 border-t border-gray-200 pt-5">
                <h3 className="text-sm font-medium text-gray-900">Author</h3>
                <div className="prose prose-sm mt-4 text-gray-500">
                  <p>{data.author}</p>
                </div>
              </div>

              <div className="mt-5 border-t border-gray-200 pt-5">
                <h3 className="text-sm font-medium text-gray-900">Status</h3>

                <div className="prose prose-sm mt-4 text-gray-500">
                  <span className="inline-flex items-center rounded-md 
                    bg-green-50 px-2 py-1 text-[14px] font-medium 
                    text-green-700 ring-1 ring-inset ring-green-600/20">
                    {data.status}
                  </span>
                </div>
              </div>
              <div className="mt-5 border-t border-gray-200 pt-5">
                <h3 className="text-sm font-medium text-gray-900">Category</h3>
                <div className="prose prose-sm mt-4 text-gray-500">
                  <p>{data.category}</p>
                </div>
              </div>

              <div className="mt-5 border-t border-gray-200 pt-5">
                <h3 className="text-sm font-medium text-gray-900">Share</h3>
                <ul role="list" className="mt-4 flex items-center space-x-6">
                  <li>
                    <a href="#" className="flex h-6 w-6 items-center justify-center text-gray-400 hover:text-gray-500">
                      <span className="sr-only">Share on Facebook</span>
                      <svg className="h-5 w-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path
                          fillRule="evenodd"
                          d="M20 10c0-5.523-4.477-10-10-10S0 4.477 0 10c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V10h2.54V7.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V10h2.773l-.443 2.89h-2.33v6.988C16.343 19.128 20 14.991 20 10z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="flex h-6 w-6 items-center justify-center text-gray-400 hover:text-gray-500">
                      <svg className="h-6 w-6" aria-hidden="true" fill="currentColor" viewBox="0 0 24 24">
                        <path
                          fillRule="evenodd"
                          d="M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="flex h-6 w-6 items-center justify-center text-gray-400 hover:text-gray-500">
                      <svg className="h-5 w-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path d="M11.4678 8.77491L17.2961 2H15.915L10.8543 7.88256L6.81232 2H2.15039L8.26263 10.8955L2.15039 18H3.53159L8.87581 11.7878L13.1444 18H17.8063L11.4675 8.77491H11.4678ZM9.57608 10.9738L8.95678 10.0881L4.02925 3.03974H6.15068L10.1273 8.72795L10.7466 9.61374L15.9156 17.0075H13.7942L9.57608 10.9742V10.9738Z" />
                      </svg>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="mx-auto mt-16 w-full max-w-2xl lg:col-span-4 lg:mt-0 lg:max-w-none">
              <Tab.Group as="div">
                <div className="border-b border-gray-200">
                  <Tab.List className="-mb-px flex space-x-8">
                    <Tab
                      className={({ selected }) =>
                        classNames(
                          selected
                            ? 'border-indigo-600 text-indigo-600'
                            : 'border-transparent text-gray-700 hover:border-gray-300 hover:text-gray-800',
                          'whitespace-nowrap border-b-2 py-6 text-sm font-medium outline-none'
                        )
                      }
                    >
                      Chapter
                    </Tab>
                  </Tab.List>
                </div>
                <Tab.Panels as={Fragment}>
                  <Tab.Panel className="-mb-5">
                    {data.chap.map((item, index) => (
                      <div key={item.id} className="flex space-x-4 text-sm text-gray-500">
                        <div className={classNames(index === 0 ? '' : 'border-t border-gray-200 ', 'py-5 w-full')}>
                          <Link to={`/detail-chap/${item.id}`} className="font-medium text-gray-900">{item.name}</Link>
                        </div>
                      </div>
                    ))}
                  </Tab.Panel>

                </Tab.Panels>
              </Tab.Group>
            </div>
          </div>
        </div>
      )}

    </div>
  );
}
