import { useEffect, useState } from "react";
import { chapRequest } from "../../../api/chapRequest";
import { useParams } from "react-router-dom";
import { IChap } from "../../../types";
export default function DetailChap() {
    const { id } = useParams();
    const [data, setData] = useState<IChap>();
    useEffect(() => {
        const getData = async () => {
            const result = await chapRequest.getDetailChap(id?.toString() || "");
            console.log("story", result);
            setData(result?.data);
        };
        getData();
    }, []);
    return (
        <div>
            {data && (
                <div className="flex justify-center">
                    <h1>{data.name}</h1>
                    <p>{data.description}</p>
                    <div className="overflow-hidden max-w-screen-md flex justify-center rounded-md bg-white shadow">
                        <ul role="list" className="divide-y divide-gray-200">
                            {data.images.map((image, index) => (
                                <li key={index} className="px-4 py-3">
                                    <img src={image} alt="img" />
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            )}
        </div>
    )
}