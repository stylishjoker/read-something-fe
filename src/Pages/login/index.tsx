import { useForm, SubmitHandler } from "react-hook-form";
import { InputController } from "../../components/InputController";
import { useAuth } from "../../hook/useAuth";
import { Link, useNavigate } from "react-router-dom";

type Props = {
  username: string;
  password: string;
};

export default function Login() {
  const navigate = useNavigate();
  const { login } = useAuth();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Props>();

  const onSubmit: SubmitHandler<Props> = async (data) => {
    try {
      const res = await login(data);
      if (res) {
        navigate("/home");
      } else {
        console.log(res);
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className="flex min-h-full flex-1 flex-col items-center justify-center px-6 py-12 lg:px-8">
        <div className="min-w-[500px] p-10 rounded-lg shadow-2xl">
          <div className="sm:mx-auto sm:w-full sm:max-w-sm">
            <img
              className="mx-auto h-10 w-auto"
              src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
              alt="Your Company"
            />
            <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
              Sign in to your account
            </h2>
          </div>

          <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
            <form className="space-y-6" onSubmit={handleSubmit(onSubmit)}>
              <InputController
                name="username"
                label="username"
                register={register}
                pattern={"{/^[a-zA-Z0-9]+$/}"}
              />
              <InputController
                name="password"
                label="password"
                register={register}
                pattern={"{/^[a-zA-Z0-9]+$/}"}
              />

              <div>
                <button
                  type="submit"
                  className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                >
                  Sign in
                </button>
              </div>
            </form>
            <div className="mt-4 flex items-center justify-center">
              <p>
                Do not have an account?
                <Link to='/register' className="ml-1 text-blue-400">
                  Register
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
