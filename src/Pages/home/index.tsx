import { useEffect } from "react";
import { useStory } from "../../hook/useStory";
import { StoryPanel } from "../../components/story-panel";
import {
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  HomeIcon,
  UsersIcon,
} from "@heroicons/react/24/outline";
function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
export default function HomePage() {
  const { story } = useStory();
  console.log(story);
  const navigation = [
    { name: "Truyện đọc nhiều nhất", href: "#", icon: HomeIcon, current: true },
    {
      name: "Lịch sử đọc truyện",
      href: "#",
      icon: CalendarIcon,
      current: false,
    },
    { name: "Tác giả", href: "#", icon: UsersIcon, current: false },
  ];
  return (
    <div className="px-4">
      <div className="flex flex-col gap-10 lg:flex-row">
        <div className="lg:w-3/4">
          <h2 className="text-lg font-medium text-blue-500 py-5">
            Truyện mới cập nhật
          </h2>
          <ul className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
            {story?.map((item, index) => (
              <li
                key={index}
                className="col-span-1 divide-y overflow-hidden divide-gray-200 rounded-lg bg-white shadow"
              >
                <StoryPanel {...item} />
              </li>
            ))}
          </ul>
        </div>
        <div className="lg:w-1/4 pt-8">
          <nav className="flex flex-1 flex-col" aria-label="Sidebar">
            <ul className="-mx-2 space-y-1">
              {navigation.map((item) => (
                <li key={item.name}>
                  <a
                    href={item.href}
                    className={classNames(
                      item.current
                        ? "bg-gray-50 text-indigo-600"
                        : "text-gray-700 hover:text-indigo-600 hover:bg-gray-50",
                      "group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold"
                    )}
                  >
                    <item.icon
                      className={classNames(
                        item.current
                          ? "text-indigo-600"
                          : "text-gray-400 group-hover:text-indigo-600",
                        "h-6 w-6 shrink-0"
                      )}
                      aria-hidden="true"
                    />
                    {item.name}
                  </a>
                </li>
              ))}
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}
