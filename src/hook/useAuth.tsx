import {
  useContext,
  useState,
  createContext,
  ReactNode,
  useEffect,
  useMemo,
} from "react";
import axios, { accessToken } from "../lib/http";
import { useCookies } from "react-cookie";
export interface AuthProps {
  id: string;
  avatar: string;
  username: string;
  isVip: number;
}

export interface AuthContextProps {
  auth?: AuthProps;
  login: (data: any) => Promise<any>;
  logout: () => void;
}

export const AuthContext = createContext<AuthContextProps>(
  {} as AuthContextProps
);

export function AuthProvider({ children }: { children: ReactNode }) {
  const [cookie, setCookie, removeCookie] = useCookies(["accessToken"]);
  const [auth, setAuth] = useState<AuthProps | undefined>();
  const [loading, setLoading] = useState<boolean>(true);

  const login = async (data: any) => {
    try {
      const res = await axios.post("/auth/login", data);
      const result = res.data;

      if (result.status === 200) {
        setCookie("accessToken", res.data.data.accessToken);
        const inforUser = (
          await axios.get("/auth/get-user", {
            headers: { Authorization: `Bearer ${res.data.data.accessToken}` },
          })
        ).data;
        if (inforUser.status === 200) {
          setAuth(inforUser?.data);
        } else {
          return false;
        }
        return true;
      } else {
        throw new Error(result.response.data || "Login failed");
      }
    } catch (e) {
      console.log(e);
    }
  };
  const logout = () => {
    window.location.href = '/home'
    removeCookie("accessToken");
    setAuth(undefined);
  };
  useEffect(() => {
    const getUser = async () => {
      try {
        setLoading(true);
        const res = await axios.get("/auth/get-user");
        console.log("user", res.data);

        setAuth(res.data.data);
      } catch (error) {
        console.error("Get user error:", error);
        logout();
      } finally {
        setLoading(false);
      }
    };

    if (cookie.accessToken) {
      getUser();
    }
  }, [accessToken]);
  const value = {
    auth,
    login,
    logout,
  };
  // if (loading) return <div>loading ...</div>;
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
export const useAuth = () => useContext(AuthContext);
