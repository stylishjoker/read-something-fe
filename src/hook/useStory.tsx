import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import axios from "../lib/http";

export interface StoryProps {
  _id: string;
  title: string;
  author: string;
  category: string[];
  follow: string[];
  status: string;
  totalView: number;
  createAt: number;
  chap: string[];
  thumbnailM: string;
  thumbnailH: string;
  image: string;
}

export interface StoryContextProps {
  story?: StoryProps[];
}

const StoryContext = createContext<StoryContextProps>({} as StoryContextProps);

export const StoryProvider = ({ children }: { children: ReactNode }) => {
  const [story, setStory] = useState<StoryProps[] | undefined>();
  useEffect(() => {
    const getData = async () => {
      try {
        const res = await axios.get("/story");
        const result = res.data;

        if (result.status === 200) {
          setStory(result?.data.items);
        }
      } catch (error) {
        console.log(error);
      }
    };
    getData();
  }, []);
  const value = {
    story,
  };
  return (
    <StoryContext.Provider value={value}>{children}</StoryContext.Provider>
  );
};
export const useStory = () => useContext(StoryContext);
