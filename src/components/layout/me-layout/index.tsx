import { ReactNode } from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../../../hook/useAuth";
import { FaRegUser } from "react-icons/fa";
import { CiBookmark } from "react-icons/ci";
import { RiLockPasswordLine } from "react-icons/ri";
import { IoLogOutOutline } from "react-icons/io5";
import { RiVipCrown2Fill } from "react-icons/ri";

export const MeLayout: React.FC<{ children: ReactNode }> = ({ children }) => {
  const { logout, auth } = useAuth();
  const currentUrl = window.location.pathname;
  const routers = [
    {
      name: "Thông tin chung",
      href: "/me",
      icon: FaRegUser,
    },
    {
      name: "Truyện theo dõi",
      href: "/me/comic-followed",
      icon: CiBookmark,
    },
    {
      name: "Đổi mật khẩu",
      href: "/me/change-password",
      icon: RiLockPasswordLine,
    },
  ];
  return (
    <div className="flex flex-col md:flex-row gap-5 pt-10">
      <div className="flex flex-col gap-4 px-10 min-w-96">
        <div className="flex items-center gap-2 border border-gray-200 rounded-md p-4 shadow-md">
          <img
            className="object-cover w-16 h-16 rounded-md"
            src={auth?.avatar}
            alt=""
          />
          <div className="flex flex-col">
            <span className="text-lg font-medium">Tài khoản của bạn</span>
            <span className="text-sm">
              {auth?.username}{" "}
              {auth?.isVip === 1 && (
                <RiVipCrown2Fill className="size-5 text-yellow-400 shadow-md" />
              )}
            </span>
          </div>
        </div>
        <nav className="flex flex-col pl-2">
          <div className="flex flex-col border-l border-gray-400 gap-1">
            {routers.map((router) => (
              <Link
                key={router.name}
                to={router.href}
                className={`${
                  currentUrl === router.href
                    ? "pl-6 bg-gray-200"
                    : "hover:bg-gray-200"
                } relative flex items-center gap-2 p-3`}
              >
                <router.icon className="size-5" /> {router.name}
                {currentUrl === router.href && (
                  <div className="absolute h-5 rounded-md top-3 w-1 bg-blue-400 left-[-2px]" />
                )}
              </Link>
            ))}
            <button
              className="flex items-center gap-2 p-3 hover:bg-gray-200"
              onClick={logout}
            >
              <IoLogOutOutline size={20} /> Đăng xuất
            </button>
          </div>
        </nav>
      </div>
      <div className="flex-1 overflow-x-hidden">{children}</div>
    </div>
  );
};
