type Props = {
  label: string;
  register: any;
  pattern: any;
  name: string;
};

export const InputController: React.FC<Props> = ({
  label,
  register,
  pattern,
  name,
}) => {
  return (
    <>
      <label className="block text-sm font-medium leading-6 text-gray-900">
        {label}
      </label>
      <div className="mt-1">
        <input
          {...register(name, { require, pattern })}
          name={label}
          autoComplete="email"
          required
          className="block w-full px-4 rounded-md border-0 py-2 outline-none text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"
        />
      </div>
    </>
  );
};
