import { StoryProps } from "../../hook/useStory";
import { Link } from "react-router-dom";

export const StoryPanel: React.FC<StoryProps> = ({ image, title, _id }) => {
  return (
    <Link to={`/detail/${_id}`}>
      <img className="w-full h-[280px]" src={image} alt="" />
      <div className="py-2 px-2">
        <span className="line-clamp-1 text-xs">{title}</span>
      </div>
    </Link>
  );
};
