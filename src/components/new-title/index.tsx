type Props = {
  title: string;
};

export const NewTitle: React.FC<Props> = ({ title }) => {
  return (
    <div className="px-2 border-l-2 border-red-500">
      <span className="text-xl font-medium">{title}</span>
    </div>
  );
};
