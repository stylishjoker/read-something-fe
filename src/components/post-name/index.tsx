type Props = {
  title: string;
};

export const PostName: React.FC<Props> = ({ title }) => {
  return (
    <div className="pr-2 relative">
      <span className="text-2xl font-semibold">{title}</span>
      <div className="absolute w-12 h-1 mt-1 bg-red-500" />
    </div>
  );
};
