import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { useAuth } from "../../../hook/useAuth";
import { Link } from "react-router-dom";
import { MdLogout } from "react-icons/md";
import { FaUser } from "react-icons/fa";
import { FaBook } from "react-icons/fa";
import { MdLogin } from "react-icons/md";

export const AvatarHeader: React.FC = () => {
  const { auth, logout } = useAuth();
  return (
    <Popover className="relative">
      <Popover.Button className="inline-flex outline-none items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900">
        <img
          className="w-12 h-12 rounded-full object-cover"
          src={auth?.avatar ? auth.avatar : require("../../../assets/user.jpg")}
        />
      </Popover.Button>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-200"
        enterFrom="opacity-0 translate-y-1"
        enterTo="opacity-100 translate-y-0"
        leave="transition ease-in duration-150"
        leaveFrom="opacity-100 translate-y-0"
        leaveTo="opacity-0 translate-y-1"
      >
        <Popover.Panel className="absolute left-1/2 z-10 mt-5 flex w-screen max-w-max -translate-x-1/2 px-4">
          <div className="w-screen gap-2 max-w-64 p-3 flex-auto flex flex-col overflow-hidden rounded-lg bg-white text-md leading-6 shadow-lg ring-1 ring-gray-900/5">
            {auth ? (
              <>
                <Link
                  className="flex items-center gap-2 hover:underline hover:pl-2"
                  to={"/me"}
                >
                  <FaUser /> Thông tin cá nhân
                </Link>
                <Link
                  className="flex gap-2 items-center hover:underline hover:pl-2"
                  to={"/me"}
                >
                  <FaBook /> Truyện đang theo dõi
                </Link>
                <button
                  className="text-start items-center flex gap-2 hover:underline hover:pl-2"
                  onClick={logout}
                >
                  <MdLogout />
                  Đăng xuất
                </button>
              </>
            ) : (
              <>
                <Link
                  className="flex items-center gap-2 hover:underline hover:pl-2"
                  to={"/login"}
                >
                  <MdLogin />
                  Đăng nhập
                </Link>
                <Link
                  className="flex items-center gap-2 hover:underline hover:pl-2"
                  to={"/login"}
                >
                  <MdLogin />
                  Đăng ký
                </Link>
              </>
            )}
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
};
