import { routers } from "../../routers";
import { AvatarHeader } from "./avatar-header";

export const Header: React.FC = () => {
  const currentUrl = window.location.pathname;
  return (
    <div className="flex fixed border-b bg-white z-10 border-gray-200 w-full py-2 items-center justify-center shadow-md">
      <div className="w-9/12 flex items-center justify-between">
        <div className="flex items-center gap-4">
          <img
            className="w-14 h-14 object-cover"
            src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
            alt="Your Company"
          />
          <div className="text-white gap-5 uppercase flex font-medium text-lg">
            {routers.map(
              (item) =>
                item.private && (
                  <a
                    className={`text-[16px] ${currentUrl === `/${item.path}` ? 'text-black' : 'text-gray-400'}`}
                    key={item.name}
                    href={`/${item.path}`}
                  >
                    {item.name}
                  </a>
                )
            )}
          </div>
        </div>
        <AvatarHeader />
      </div>
    </div>
  );
};
