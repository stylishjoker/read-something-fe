/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        "bg-header":
          "url('/Users/kien/workspace/sources/read-something-fe/src/assets/bg_header.png')",
      },
    },
  },
  plugins: [],
};
